{-# LANGUAGE BlockArguments #-}

module Brainfuck.Main where

import qualified Brainfuck.Codegen          as JIT
import qualified Brainfuck.Interpreter      as I
import           Brainfuck.Language         (Program, brainfuck)
import           Data.Attoparsec.ByteString (endOfInput, parseOnly)
import qualified Data.ByteString.Char8      as BS
import           System.IO                  (BufferMode (NoBuffering),
                                             hSetBuffering, stdout)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  getArgs >>= traverse_ runBrainfuckFile

parseFile :: FilePath -> IO (Either String Program)
parseFile file = parseOnly (brainfuck <* endOfInput) <$> BS.readFile file

runBrainfuckFile :: FilePath -> IO ()
runBrainfuckFile file = do
  result <- parseFile file
  case result of
    Left msg -> putStrLn msg
    Right program -> do
      -- I.runBrainfuck program
      JIT.runBrainfuck program
