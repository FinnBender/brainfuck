{-# LANGUAGE DeriveDataTypeable #-}

module Brainfuck.Language where

import           Data.Attoparsec.ByteString.Char8 (Parser, char, choice,
                                                   skipWhile, takeWhile1)
import qualified Data.ByteString.Char8            as BS

type Program = [Command]

data Command
  = IncPtr !Int
  | DecPtr !Int
  | IncVal !Word8
  | DecVal !Word8
  | PutChar
  | GetChar
  | Loop Program
  | SetVal !Word8
  deriving (Show, Data)

validBrainfuckChar :: Char -> Bool
validBrainfuckChar c = c `elem` "><.,+-[]"

token :: Parser a -> Parser a
token parser = spaces *> parser

spaces :: Parser ()
spaces = skipWhile (not . validBrainfuckChar)

simpleCommand :: Parser Command
simpleCommand =
  token $
    choice
      [ IncPtr <$> count1 '>'
      , DecPtr <$> count1 '<'
      , IncVal <$> count1 '+'
      , DecVal <$> count1 '-'
      , PutChar <$ char '.'
      , GetChar <$ char ','
      ]

whileLoop :: Parser Command
whileLoop = Loop <$> token (char '[' *> brainfuck <* char ']')

brainfuck :: Parser Program
brainfuck = many (simpleCommand <|> whileLoop) <* spaces

count1 :: Integral a => Char -> Parser a
count1 c = fromIntegral . BS.length <$> takeWhile1 (== c)

pretty :: Command -> String
pretty (IncPtr n)     = replicate n '>'
pretty (DecPtr n)     = replicate n '<'
pretty (IncVal n)     = replicate (fromIntegral n) '+'
pretty (DecVal n)     = replicate (fromIntegral n) '-'
pretty PutChar        = "."
pretty GetChar        = ","
pretty (SetVal n)     = "(" <> show n <> ")"
pretty (Loop program) = "[" <> foldMap pretty program <> "]"
