{-# LANGUAGE BlockArguments     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE RecursiveDo        #-}

module Brainfuck.Codegen where

import           Brainfuck.Language        (Command (..), Program)
import           Brainfuck.Optimizer       (optimizeProgram)
import           LLVM.AST                  (Module, Name, Operand)
import           LLVM.AST.IntegerPredicate (IntegerPredicate (EQ, SGE))
import           LLVM.AST.Type             (Type (VoidType), i64, i8, ptr)
import           LLVM.Context              (withContext)
import           LLVM.ExecutionEngine      (ExecutionEngine (getFunction, withModuleInEngine),
                                            withMCJIT)
import           LLVM.IRBuilder            (IRBuilderT, ModuleBuilder, add,
                                            alloca, block, br, buildModule,
                                            call, condBr, extern, function,
                                            icmp, int64, int8, load, named, phi,
                                            retVoid, store, sub, trunc, zext)
import           LLVM.Module               (withModuleFromAST)
import           Prelude                   hiding (EQ, modify)

type MemPtr = Operand

type PrevBlock = Name

type NextBlock = Name

type Compile = IRBuilderT ModuleBuilder

data PrimOps = PrimOps
  { incPtr  :: Operand
  , decPtr  :: Operand
  , incVal  :: Operand
  , decVal  :: Operand
  , setVal  :: Operand
  , putchar :: Operand
  , getchar :: Operand
  }

runBrainfuck :: Program -> IO ()
runBrainfuck program =
  withContext \ctx ->
    withMCJIT ctx (Just 2) Nothing Nothing Nothing \jit ->
      withModuleFromAST ctx (compileToIR $ optimizeProgram program) \module' ->
        withModuleInEngine jit module' \ee ->
          getFunction ee "main" >>= \case
            Nothing -> putStrLn "sorry no fun"
            Just f  -> runFunPtr f

foreign import ccall "dynamic" unFunPtr :: FunPtr (IO ()) -> IO ()

runFunPtr :: FunPtr () -> IO ()
runFunPtr = unFunPtr . castFunPtr

compileToIR :: Program -> Module
compileToIR program = buildModule "brainfuck" $ buildProgram program

buildProgram :: Program -> ModuleBuilder Operand
buildProgram program = do
  primOps <- declarePrimOps
  function "main" [] VoidType \[] -> do
    let len = int64 50000
    mem <- alloca i8 (Just len) 1
    pos <- alloca (ptr i8) Nothing 8
    store pos 1 mem
    clearBytes mem len
    traverse_ (buildCommand pos primOps) program
    retVoid

declarePrimOps :: ModuleBuilder PrimOps
declarePrimOps = do
  incPtr <- function "incPtr" [(ptr (ptr i8), "pos'"), (i64, "n")] VoidType \[pos', n] -> do
    pos <- load pos' 8
    newPos <- add pos n
    store pos' 8 newPos
  decPtr <- function "decPtr" [(ptr (ptr i8), "pos'"), (i64, "n")] VoidType \[pos', n] -> do
    pos <- load pos' 8
    newPos <- sub pos n
    store pos' 8 newPos
  incVal <- function "incVal" [(ptr (ptr i8), "pos'"), (i8, "n")] VoidType \[pos', n] -> do
    pos <- load pos' 8
    val <- load pos 1
    newVal <- add val n
    store pos 1 newVal
  decVal <- function "decVal" [(ptr (ptr i8), "pos'"), (i8, "n")] VoidType \[pos', n] -> do
    pos <- load pos' 8
    val <- load pos 1
    newVal <- sub val n
    store pos 1 newVal
  setVal <- function "setVal" [(ptr (ptr i8), "pos'"), (i8, "n")] VoidType \[pos', n] -> do
    pos <- load pos' 8
    store pos 1 n
  putchar <- function "putChar" [(ptr (ptr i8), "pos'")] VoidType \[pos'] -> do
    pos <- load pos' 8
    val <- load pos 1
    val64 <- zext val i64
    putc <- extern "putchar" [i64] VoidType
    _ <- call putc [(val64, [])]
    pure ()
  getchar <- function "getChar" [(ptr (ptr i8), "pos'")] VoidType \[pos'] -> do
    pos <- load pos' 8
    getc <- extern "getchar" [] i64
    val64 <- call getc []
    val <- trunc val64 i8
    store pos 1 val
  pure PrimOps{..}

buildCommand :: MemPtr -> PrimOps -> Command -> Compile ()
buildCommand pos' PrimOps{..} cmd = case cmd of
  IncPtr n -> void $ call incPtr [(pos', []), (int64 $ toInteger n, [])]
  DecPtr n -> void $ call decPtr [(pos', []), (int64 $ toInteger n, [])]
  IncVal n -> void $ call incVal [(pos', []), (int8 $ toInteger n, [])]
  DecVal n -> void $ call decVal [(pos', []), (int8 $ toInteger n, [])]
  SetVal n -> void $ call setVal [(pos', []), (int8 $ toInteger n, [])]
  PutChar -> void $ call putchar [(pos', [])]
  GetChar -> void $ call getchar [(pos', [])]
  Loop program -> mdo
    br loopHead
    loopHead <- block `named` "loopHead"
    pos <- load pos' 8
    val <- load pos 1
    finished <- icmp EQ val (int8 0)
    condBr finished loopFoot loopBody
    loopBody <- block `named` "loopBody"
    traverse_ (buildCommand pos' PrimOps{..}) program
    br loopHead
    loopFoot <- block `named` "loopFoot"
    pure ()

clearBytes :: Operand -> Operand -> Compile ()
clearBytes arrayHead arrayLen = mdo
  br initLoop
  initLoop <- block `named` "clearBytes"
  br beginLoop
  beginLoop <- block
  i <- phi [(int64 0, initLoop), (inext, beginLoop)]
  arrayItem <- add arrayHead i
  store arrayItem 1 (int8 0)
  inext <- add i (int64 1)
  finished <- icmp SGE inext arrayLen
  condBr finished endLoop beginLoop
  endLoop <- block
  pure ()
