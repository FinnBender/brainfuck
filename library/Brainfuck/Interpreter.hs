{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE LambdaCase       #-}

module Brainfuck.Interpreter where

import           Brainfuck.Language          (Command (DecPtr, DecVal, GetChar, IncPtr, IncVal, Loop, PutChar, SetVal),
                                              Program)
import           Brainfuck.Optimizer         (optimizeProgram)
import           Data.Vector.Unboxed.Mutable (IOVector)
import qualified Data.Vector.Unboxed.Mutable as M
import           Prelude                     hiding (left, read, right)

data Memory = Memory !Int (IOVector Word8)


leftBy :: Int -> Memory -> Memory
leftBy m (Memory n vec) = Memory (n - m) vec

rightBy :: Int -> Memory -> Memory
rightBy m (Memory n vec) = Memory (n + m) vec

incBy :: Word8 -> Memory -> IO ()
incBy m (Memory n vec) = M.modify vec (+ m) n

decBy :: Word8 -> Memory -> IO ()
decBy m (Memory n vec) = M.modify vec (\x -> x - m) n

write :: Memory -> Word8 -> IO ()
write (Memory n vec) = M.write vec n

read :: Memory -> IO Word8
read (Memory n vec) = M.read vec n

interpretCommand :: Memory -> Command -> IO Memory
interpretCommand mem = \case
  IncPtr n      -> pure $ rightBy n mem
  DecPtr n      -> pure $ leftBy n mem
  IncVal n      -> mem <$ incBy n mem
  DecVal n      -> mem <$ decBy n mem
  SetVal n      -> mem <$ write mem n
  PutChar       -> mem <$ (putChar . unsafeCoerce =<< read mem)
  GetChar       -> mem <$ (write mem . unsafeCoerce =<< getChar)
  Loop program' -> iterateUntilM ptrIsZero (`interpret` program') mem

ptrIsZero :: Memory -> IO Bool
ptrIsZero mem = (== 0) <$> read mem

interpret :: Memory -> Program -> IO Memory
interpret = foldM interpretCommand

runBrainfuck :: Program -> IO ()
runBrainfuck program = do
  vec <- M.replicate 50000 0
  void $ interpret (Memory 0 vec) (optimizeProgram program)

iterateUntilM :: Monad m => (a -> m Bool) -> (a -> m a) -> a -> m a
iterateUntilM predM f = go
  where
    go a =
      predM a >>= \case
        True  -> pure a
        False -> f a >>= go
