module Brainfuck.Optimizer where

import           Brainfuck.Language          (Command (DecVal, IncVal, Loop, SetVal),
                                              Program)
import           Data.Generics.Uniplate.Data (transform)


optimizeSetZero :: Program -> Program
optimizeSetZero = transform optimize
 where
  optimize (Loop [DecVal 1] : rest) = SetVal 0 : rest
  optimize (Loop [IncVal 1] : rest) = SetVal 0 : rest
  optimize program                  = program

optimizeSetVal :: Program -> Program
optimizeSetVal = transform optimize
 where
  optimize (SetVal _ : SetVal m : rest) = SetVal m : rest
  optimize (IncVal _ : SetVal m : rest) = SetVal m : rest
  optimize (DecVal _ : SetVal m : rest) = SetVal m : rest

  optimize (SetVal n : IncVal m : rest) = SetVal (n+m) : rest
  optimize (SetVal n : DecVal m : rest) = SetVal (n-m) : rest

  optimize (IncVal n : IncVal m : rest) = IncVal (n+m) : rest
  optimize (IncVal n : DecVal m : rest)
    | n > m = IncVal (n-m) : rest
    | n < m = DecVal (m-n) : rest
    | otherwise = rest
  optimize (DecVal n : IncVal m : rest)
    | n < m = IncVal (m-n) : rest
    | n > m = DecVal (m-n) : rest
    | otherwise = rest
  optimize (DecVal n : DecVal m : rest) = DecVal (n+m) : rest

  optimize program = program


optimizeProgram :: Program -> Program
optimizeProgram program
  = program
  & optimizeSetZero
  & optimizeSetVal
