module Main where

import qualified Brainfuck.Main as Brainfuck

main :: IO ()
main = Brainfuck.main
